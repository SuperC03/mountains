import java.util.*;
import java.io.*;
import java.awt.*;

public class MapDataDrawer
{

  private int[][] grid;

  public MapDataDrawer(String filename, int rows, int cols){
      try {
          // initialize grid 
          grid = new int[rows][cols];
          // Setup File Reader
          File file = new File(filename);
          Scanner input = new Scanner(file);
          //read the data from the file into the grid
          for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                grid[i][j] = input.nextInt();
            }
          }
      } catch (Exception ex){
          ex.printStackTrace();
      }
  }
  
  /**
   * @return the min value in the entire grid
   */
  public int findMinValue(){
    int min = grid[0][0];
    for(int i = 0; i < grid.length; i++) {
        for(int j = 0; j < grid[i].length; j++) {
            if(grid[i][j] < min) { min = grid[i][j]; } 
        }
    }
    return min;    
  }
  /**
   * @return the max value in the entire grid
   */
  public int findMaxValue(){
    int max = grid[0][0];
    for(int i = 0; i < grid.length; i++) {
        for(int j = 0; j < grid[i].length; j++) {
            if(grid[i][j] > max) { max = grid[i][j]; } 
        }
    }
    return max;
  }
  
  /**
   * @param col the column of the grid to check
   * @return the index of the row with the lowest value in the given col for the grid
   */
  public int indexOfMinInCol(int col){
      int min = grid[0][col];
      int index = 0;
      for(int i = 0; i < grid.length; i++) {
          if(grid[i][col] < min) {
              min = grid[i][col];
              index = i;
          }
      }
      return index;
  }
  /**
   * @param x Input to Convert
   * @param in_min Min Value of Input Range
   * @param in_max Max Value of Input Range
   * @param out_min Min Value of Output Range
   * @param out_max Max Value of Output Range
   * @returns Mapped Value
   */
  public int map(double x, double in_min, double in_max, double out_min, double out_max) {
      return (int) ((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
  }
  /**
   * Draws the grid using the given Graphics object.
   * Colors should be grayscale values 0-255, scaled based on min/max values in grid
   */
  public void drawMap(Graphics g){
      int min = this.findMinValue();
      int max = this.findMaxValue();
      for(int i = 0; i < grid.length; i++) {
          for(int j = 0; j < grid[i].length; j++) {
              int c = map(grid[i][j], min, max, 30, 255);
              g.setColor(new Color(c, c, c));
              g.fillRect(j, i, 1, 1);
          }
      }      
  }
  /**
   * @param point1 First Value
   * @param point2 Second Value
   * @returns Distance Between the Two Points
   */
  public int distBetween(int point1, int point2) {
      return Math.abs(point2 - point1);
  }
  /**
   * Find a path from West-to-East starting at given row.
   * Choose a foward step out of 3 possible forward locations, using greedy method described in assignment.
   * @return the total change in elevation traveled from West-to-East
   */
  public int drawLowestElevPath(Graphics g, int row){
    // Total Incline
    int totalIncline = 0;
    g.fillRect(0, row, 1, 1);
    for(int col = 1; col < this.grid[0].length - 1; col++) {
        // Check if the Line Ran Into the Edge, in Which Case Break It
        if(row <= 0 || row >= this.grid.length - 1) {
            return Integer.MAX_VALUE;
        }
        // Distances
        int upDist = this.distBetween(this.grid[row][col-1], this.grid[row - 1][col]);
        int midDist = this.distBetween(this.grid[row][col-1], this.grid[row][col]);
        int downDist = this.distBetween(this.grid[row][col-1], this.grid[row + 1][col]);
        if((upDist == midDist && midDist <= downDist) || (downDist == midDist && midDist <= upDist)) {
            row = row;
            totalIncline += midDist;
        } else if(upDist == downDist && upDist < midDist) {
            row = row + (Math.random() <= .5 ? 1 : -1);
            totalIncline += upDist;
        } else if(upDist < midDist && upDist < downDist) {
            row = row - 1;
            totalIncline += upDist;
        } else if(downDist < midDist && downDist < upDist) {
            row = row + 1;
            totalIncline += downDist;
        } else {
            row = row;
            totalIncline += midDist;
        }
        g.fillRect(col, row, 1, 1);
    }
    return totalIncline;
  }
  
  /**
   * @return the index of the starting row for the lowest-elevation-change path in the entire grid.
   */
  public int indexOfLowestElevPath(Graphics g){
     // Lowest Elevation Change among Rows
     int lowest = Integer.MAX_VALUE;
     // Index of Starting Point on Row with Lowest Overall Elevation
     int indexOfLowest = 1;
     // Loop Over Every Row (Not The First One Though...)
     for(int i = 1; i < this.grid.length - 1; i++) {
         int change = this.drawLowestElevPath(g, i);
         if(change < lowest) {
             lowest = change;
             indexOfLowest = i;
         }
     } 
     return indexOfLowest;
  }
  
  public void test() {
      for(int i = 0; i < grid.length; i++) {
          for(int j = 0; j < grid[i].length; j++) {
              System.out.print(grid[i][j] + " ");
          }
          System.out.println();
      }
  }
}